# BMIG 5003 Lectures Repository #

Welcome to BMIG 5003 Fall 2021

We will keep our lectures, code and jupyter lab notebooks in this repository.

## Notes for this repository

You should clone this repository into your local drive. There are a couple of options

* The simplest way would be using [source tree](https://www.sourcetreeapp.com/)

>Do not expect that this repository automatically synchronizes with your computer, you need to keep it up to date with the *pull* command.

Once you are confortable cloning this repository and getting familiar with *pull* we will move to

* The [official git](https://git-scm.com/) which works from the command line

Hello from bitbucket

