* [Directory Structure](#markdown-header-directory-structure)

	* [Run the lecture](#markdown-header-run-the-lecture)
	* [PyCharm vs Jupyter Lab](#markdown-header-pycharm-vs-jupyter-lab)


# Directory Structure

The lectures will be located in this folder. 
There will be three types of files

+ Jupyter lab (notebook) files with extension *.ipynb
+ Additional python files with extension *.py
+ Lecture PDF file converted from the above file (this file may differ from the above file)
+ Data files as required (*.csv, *.json).
+ Lecture TeX file (you don't have to worry about these files as they are only useful to your instructor)

##  Run the Lecture

If you are using a Mac you should open a _Terminal_ whereas for windows you should 
use _Anaconda Prompt_.

1. Open a terminal that has conda installed. The word _(base)_ should appear in front of your prompt.    
2. Move to the directory where your files are located (use the _cd_ command). That is, you should be located inside teh Students_BMIG_5003 folder.
3. Invoke the following command (wihtout the _>_) 
```bash
> jupyter lab
```
4. You should be able to see a browser windows open.
5. Inside the browser you will be able to see the directory structure.
6. Click on the corresponding lecture (ipynb) file and it will show on your screen
7. There is a buttom to run specific _cells_.
8. Do not foget to use the _Shutdown_ (under the _File_ option) to close your kernel. Otherwise, your computer will be using resources not needed.

## PyCharm vs Jupyter Lab

In this course, we will be programming in Python and learning the basics requires the use of an appropriate 
IDE (Pycharm or Spyder). 
Jupyter Labs are useful and handle everything _behind scenes_ so it limits the real programming experience for novice coders.

> I expect you to use PyCharm or similar for most of this course. 
